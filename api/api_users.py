from web_setting import flask_app, ma, db
from models.all_models import Users, TagsUsers, Tags

from api.api_tags import TagsGlobalSchema


class TagsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TagsUsers
        load_instance = True
        include_relationships = True

    connect_tags = ma.Nested(TagsGlobalSchema(only=['tag_name',]))

class UsersSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Users
        load_instance = True
        include_relationships = True

    users_tagsusers = ma.List(ma.Nested(TagsSchema(exclude=('connect_users', 'id',))))


def _base_query():
    return db.session.query(Users)

def _base_query_user_tag():
    return db.session.query(TagsUsers.user_id, TagsUsers.tags_id, Tags.tag_name).join(Tags)

#
def all_users():
    data_query = _base_query()
    api_all_users = UsersSchema(many=True)

    return api_all_users.dump(data_query)


def filter_users(user_id=None):
    data_query = _base_query()

    if user_id is not None:
        data_query = data_query.filter(Users.id == user_id)

    api_all_users = UsersSchema()
    return api_all_users.dump(data_query)


