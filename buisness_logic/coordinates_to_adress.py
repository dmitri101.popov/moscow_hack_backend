from geopy.geocoders import Nominatim
import json


geolocator = Nominatim(user_agent='volunteer')


def location_from_address(default_location='Москва, Беговая, 17 к1'):
    try:
        location = geolocator.geocode(default_location)

        if location is None:
            return json.dumps("Cannot find this address")
        return location.address, location.latitude, location.longitude

    except:
        return json.dumps("Cannot make location from address")


def location_from_coordinates(default_coordinates="52.509669, 13.376294"):
    try:
        location = geolocator.reverse(default_coordinates)
        if location is None:
            return json.dumps("Wrong coordinates")
        return location.address, location.latitude, location.longitude

    except:
        return json.dumps(f"Cannot make location from coordinates ({default_coordinates})")


def location_from_address(default_location='Москва, Беговая, 17 к1'):
    try:
        location = geolocator.geocode(default_location)

        if location is None:
            return json.dumps("Cannot find this address")

        return location.address, location.latitude, location.longitude

    except:
        return json.dumps("Cannot make location from address")


def location_from_coordinates(default_coordinates="52.509669, 13.376294"):
    try:
        location = geolocator.reverse(default_coordinates)

        if location is None:
            return json.dumps("Wrong coordinates")

        return location.address, location.latitude, location.longitude
    except:
        return json.dumps(f"Cannot make location from coordinates ({default_coordinates})")
