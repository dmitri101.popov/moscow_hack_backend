from web_setting import flask_app

from routers import entry_routers, \
    admin_routers, users_routers, advertisement_routers, community_routers

if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', debug=True)