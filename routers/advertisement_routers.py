from web_setting import flask_app, cross_origin
from api.api_advertisment import all_advertisments, indexes_advertisments, filter_advertisments
from api.api_customers import filter_customers
from pydantic import BaseModel, constr
from typing import Optional
from flask import jsonify, request

from routers.all_dataclasses import TagsIndex, FilterRad
from buisness_logic.insert_models import add_new_click
from recomendation_system import get_recomendation_by_tags, get_recomendation_by_location, get_adv_in_radius


@flask_app.route('/api/advertisement', methods=["POST", "GET"])
def advertisement():
    if request.method == 'GET':
        return jsonify(all_advertisments())
    if request.method == 'POST':
        return jsonify('XER')


@flask_app.route('/api/advertisement/get/<int:adv_id>/', methods=["GET", "POST"])
def get_advertisement(adv_id=None):
    js_result = filter_advertisments(id_adv=adv_id)

    if request.method == 'POST':
        user_id = request.json.get('user_id')

        for _ in js_result:
            tag_id = []
            for val in _['adv_id_tagsadv']:
                tag_id.append(val['connect_tags']['id'])
            con_tags = TagsIndex(tags_id=tag_id, index_connect=user_id)
            add_new_click(con_tags)

    return jsonify(js_result)


@flask_app.route('/api/advertisement/recommendations/<int:user_id>/', methods=["GET"])
def rec_advertisement(user_id=None):

    if user_id is not None:
        advertisments_ids = get_recomendation_by_tags(user_id)
        return jsonify(indexes_advertisments(advertisments_ids))

    return jsonify('Error rec_advertisement'), 401


@flask_app.route('/api/advertisement/recommendations_geo/', methods=["POST"])
def rec_geo_advertisement(user_id=None):
    user_id = request.json.get('user_id')
    coordinate = request.json.get('coodinate')

    if user_id is not None:
        if coordinate is None:
            try:
                coordinate = filter_customers(user_id=user_id)[0]['coordinates']
            except:
                jsonify('Error rec_geo_advertisement'), 401

        advertisments_ids = get_recomendation_by_location(user_id, coordinate)
        return indexes_advertisments(advertisments_ids)

    return jsonify('Error rec_geo_advertisement'), 401


@flask_app.route('/api/advertisement/filter/radius/', methods=["POST"])
def routers_advertisement():
    if request.method == 'POST':
        data_user = FilterRad(**request.get_json())

        if data_user.coordinate is None:
            data_user.coordinate = filter_customers(user_id=data_user.user_id)[0]['coordinates']

        try:
            id_adv = get_adv_in_radius(FilterRad.coordinate, FilterRad.rad)
        except:
            jsonify('Error routers_advertisement'), 401

        return jsonify(indexes_advertisments(id_adv))


@flask_app.route('/api/advertisement/create/<string:adv_name>_<string:time_event>_<string:type_event>_<int:employer_id>_<int:coordinates>_<int:categories_id>_<int:wait_size_users>/', methods=["GET","POST"])
def create_adv(adv_name=None, time_event=None, type_event=None, employer_id=None, coordinates=None, categories_id=None, wait_size_users=None):
    if adv_name is not None and time_event is not None and type_event is not None and employer_id is not None and coordinates_id is not None and categories_id is not None and wait_size_users:
        print('\n\n', time_event)
        parsed_time_event = datetime.strptime(time_event, '%H:%M-%d-%m-%Y')

        address = location_from_coordinates(coordinates)


        create_advertisement(adv_name, parsed_time_event, type_event, employer_id, coordinates_id, categories_id, wait_size_users)
        return jsonify("event created successfully")

    return jsonify('Error'), 401    
