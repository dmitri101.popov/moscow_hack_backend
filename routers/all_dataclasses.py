from pydantic import BaseModel, constr
from typing import Optional, List


class EntryDate(BaseModel):
    """Описание JSON ввиде модели, что-то вроде датаклассов,
    Эти поля Федя отправляет с фронта"""

    user_name: Optional[str] = None
    login: constr(min_length=2, max_length=20)
    password: constr(min_length=7, max_length=100)
    role: Optional[int] = None
    email: Optional[str] = None
    coordinat: Optional[str] = None
    description: Optional[str] = None

class TagsIndex(BaseModel):
    index_connect: int
    tags_id: List[int]
    tags_name: Optional[str] = None


class FilterRad(BaseModel):
    user_id: int
    rad: float
    coordinate: Optional[str] = None


class RepostEvent(BaseModel):
    custimers_id: int
    adv_id: int
    is_repost: bool
    community_id: int

class CustCommunity(BaseModel):
    status: str
    community_id: int
    customers_id: int
