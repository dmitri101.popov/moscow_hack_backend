from web_setting import db


class Tags(db.Model):
    """Все теги"""
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    tag_name = db.Column(db.String)

    def __init__(self, new_name):
        self.tag_name = new_name

    def __repr__(self):
        return f"Tag name:'{self.tag_name}'"


ROLE_EMPLOYER = 0
ROLE_CUSTOMERS = 1

class Users(db.Model):
    """Пользователи"""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_name = db.Column(db.String)
    login = db.Column(db.String, unique=True, nullable=False)
    password = db.Column(db.Unicode, nullable=False)
    role = db.Column(db.Integer)
    email = db.Column(db.String)

    def __init__(self, new_name, new_login, password, new_email, role=ROLE_EMPLOYER):
        self.user_name = new_name
        self.role = role
        self.login = new_login
        self.password = password
        self.email = new_email

    # def __init__(self, **kwargs):
    #     super(Users, self).__init__(**kwargs)

    def __repr__(self):
        return f"User name:'{self.user_name}', login {self.login}"

    @staticmethod
    def _bootstrap(count=100):
        from mimesis.providers import Person
        from mimesis.locales import Locale
        from mimesis.enums import Gender

        person = Person(locale=Locale.RU)

        for _ in range(count):
            from random import randint
            gender = randint(0, 1)
            role = randint(0, 1)
            if gender == 1:
                gender = Gender.MALE
            else:
                gender = Gender.FEMALE
            user = Users(user_name=person.full_name(gender), login=person.username(), password=person.password(), email=person.email(), role=role)
            db.session.add(user)
            try:
                db.session.commit()
            except Exception:
                db.session.rollback()


class Categories(db.Model):
    """Все категории"""
    __tablename__ = 'categories'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String)

    def __init__(self, new_name):
        self.name = new_name

    def __repr__(self):
        return f"Categories name:'{self.name}"


class Employer(db.Model):
    __tablename__ = 'employer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    discription = db.Column(db.String)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    connect_users = db.relationship('Users', backref='users_emp')

    def __init__(self, user_id, new_discription=None):
        self.user_id = user_id
        self.discription = new_discription

    def __repr__(self):
        return f"Employer name:'{self.connect_users}"


class Community(db.Model):
    __tablename__ = 'community'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    community_name = db.Column(db.String)
    community_description = db.Column(db.Text)


    def __init__(self, new_community_name, new_community_description):
        self.community_name = new_community_name
        self.community_description = new_community_description


class Customers(db.Model):
    __tablename__ = 'customers'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    coordinates = db.Column(db.String)
    points = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    connect_users = db.relationship('Users',  backref='users_cus', uselist=False)

    def __init__(self, coords, user_id, points_amount = 0):
        self.user_id = user_id
        self.coordinates = coords
        self.points = points_amount

    def __repr__(self):
        return f"Customers name:'{self.connect_users}'"


class CustomersCommunity(db.Model):
    __tablename__ = 'cust_community'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    customers_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    connect_cust = db.relationship('Customers', backref='customers_comm')

    community_id = db.Column(db.Integer, db.ForeignKey('community.id'))
    connect_com = db.relationship('Community', backref='community_cus')

    def __int__(self, customers_id, community_id):
        self.community_id = community_id
        self.customers_id = customers_id


class СoordinatesEvent(db.Model):
    __tablename__ = 'coordinates_event'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    address = db.Column(db.String, nullable=False)
    coordinates = db.Column(db.String, nullable=False)

    def __int__(self, address, coordinates):
        self.address = address
        self.coordinates = coordinates

    def __repr__(self):
        return f"СoordinatesEvent name:'{self.address}"


default_confirmed_status = False
class Advertisement(db.Model):
    __tablename__ = 'advertisement'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    adv_name = db.Column(db.String)
    end_event = db.Column(db.DateTime)
    type_event = db.Column(db.String)
    time_create = db.Column(db.DateTime)
    discription = db.Column(db.Text)
    status_end = db.Column(db.Boolean)

    wait_size_users = db.Column(db.Integer)

    employer_id = db.Column(db.Integer, db.ForeignKey('employer.id'))
    connect_employer = db.relationship('Employer', backref='advertisement_employer')

    coordinates_id = db.Column(db.Integer, db.ForeignKey('coordinates_event.id'))
    connect_coordinates = db.relationship('СoordinatesEvent', backref='categories_coordinates', uselist=False)

    categories_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    connect_categories = db.relationship('Categories', backref='categories_advertisement', uselist=False)

    def __init__(self, new_adv_name, new_employer_id, new_coordinates, new_categories_id, new_tags_id,
                 new_confirmed=default_confirmed_status):
        self.adv_name = new_adv_name
        self.employer_id = new_employer_id
        self.confirmed = new_confirmed
        self.coordinates = new_coordinates
        self.categories_id = new_categories_id
        self.tags_id = new_tags_id

    def __repr__(self):
        return f"Advertisement name:'{self.adv_name}"

default_status = False
class ApprovedCustomer(db.Model):
    __tablename__ = 'approved_customer'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    status = db.Column(db.Boolean)
    adv_id = db.Column(db.Integer, db.ForeignKey('advertisement.id'))
    connect_adv = db.relationship('Advertisement', backref='adv_id_approvedcustomer')

    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id'))
    connect_customer = db.relationship('Customers', backref='customer_approve')

    def __init__(self, new_customer_id, new_status=default_status):
        self.customer_id = new_customer_id
        self.status = new_status


class CommunityAdv(db.Model):
    __tablename__ = 'community_adv'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    community_id = db.Column(db.Integer, db.ForeignKey('community.id'))
    connect_community_id = db.relationship('Community', backref='comm_adv_comm_id_community')

    adv_id = db.Column(db.Integer, db.ForeignKey('advertisement.id'))
    connect_adv_id = db.relationship('Advertisement', backref='comm_adv_adv_id_advertisements')

    is_repost = db.Column(db.Boolean)

    reposted_by = db.Column(db.Integer, db.ForeignKey('customers.id'))
    connect_reposted_by = db.relationship('Customers', backref='comm_adv_reposted_by_customers')

    def __init__(self, new_community_id, new_adv_id, new_is_repost, new_reposted_by):
        self.community_id = new_community_id
        self.adv_id = new_adv_id
        self.is_repost = new_is_repost
        self.reposted_by = new_reposted_by


class TagsCategories(db.Model):
    """Вспомагательная таблица для связи категории и тегов"""
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    categories_id = db.Column(db.Integer, db.ForeignKey('categories.id'))
    connect_cat = db.relationship('Categories', backref='categories_cat')

    tags_id = db.Column(db.Integer, db.ForeignKey('tags.id'))
    connect_tags = db.relationship('Tags', backref='cat_tags')


class TagsCommunity(db.Model):
    __tablename__ = 'tags_community'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    tags_id = db.Column(db.Integer, db.ForeignKey('tags.id'))
    connect_tags = db.relationship('Tags', backref='tags_community')

    community_id = db.Column(db.Integer, db.ForeignKey('community.id'))
    connect_community = db.relationship('Community', backref='tags_community_community')

    def __init__(self, new_tag_name, new_community_id):
        self.tag_name = new_tag_name
        self.community_id = new_community_id


class TagsAdvertisement(db.Model):
    """Вспомагаткльная таблица для связи Событий и тегов"""
    __tablename__ = 'tags_advertisement'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    adv_id = db.Column(db.Integer, db.ForeignKey('advertisement.id'))
    connect_adv_id = db.relationship('Advertisement', backref='adv_id_tagsadv')

    tags_id = db.Column(db.Integer, db.ForeignKey('tags.id'))
    connect_tags = db.relationship('Tags', backref='tags_tagsadv')

    def __init__(self):
        pass


class TagsUsers(db.Model):
    """Вспомагаткльная таблица для связи Пользователей и тегов"""
    __tablename__ = 'tags_users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    connect_users = db.relationship('Users', backref='users_tagsusers')

    tags_id = db.Column(db.Integer, db.ForeignKey('tags.id'))
    connect_tags = db.relationship('Tags', backref='tags_tagsusers')

    def __init__(self):
        pass



class ClickHistory(db.Model):
    """Вспомагаткльная таблица для связи Пользователей и тегов"""
    __tablename__ = 'click_history'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    connect_users = db.relationship('Users', backref='users_click')

    tags_id = db.Column(db.Integer, db.ForeignKey('tags.id'))
    connect_tags = db.relationship('Tags', backref='tags_clicks')

    def __init__(self, user_id, tags_id):
        self.user_id = user_id
        self.tags_id = tags_id


